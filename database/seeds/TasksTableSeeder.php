<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            [
                'title' => 'test task 1',
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => 1,
                'status' => '0',
                   ],
                   [
                    'title' => 'test task2',
                    'created_at' => date('Y-m-d G:i:s'),
                    'user_id' => 2,
                    'status' => '0',
                       ],
                       [
                        'title' => 'test task3',
                        'created_at' => date('Y-m-d G:i:s'),
                        'user_id' => 3,
                        'status' => '0',
                           ],
           
                ]);
                }
    }

