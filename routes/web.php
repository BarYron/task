<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('tasks/filter/', 'TaskController@indexFiltered')->name('myfilter');



Route::get('tasks/needed/', 'TaskController@need')->name('need');


Route::resource('tasks', 'TaskController')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('tasks/done/{id}', 'TaskController@done')->name('done');

Route::get('tasks/undone/{id}', 'TaskController@undone')->name('undone');

Route::get('tasks/delete/{id}', 'TaskController@destroy')->name('delete');


