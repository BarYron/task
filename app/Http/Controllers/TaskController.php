<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;



class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     */
    public function change_status($id,$status)
    {
       return view('tasks.index', ['id'=>$id, 'status'=>$status]);
    }    

     public function done($id)
     {
         //only if this todo belongs to user 
         if (Gate::denies('admin')) {
             abort(403,"You are not allowed to mark tasks as dome..");
          }          
         $task = Task::findOrFail($id);            
         $task->status = 1; 
         $task->save();
         return redirect('tasks');    
     }    

     public function undone($id)
     {
         //only if this todo belongs to user 
         if (Gate::denies('admin')) {
             abort(403,"You are not allowed to mark tasks as dome..");
          }          
         $task = Task::findOrFail($id);            
         $task->status = 0; 
         $task->save();
         return redirect('tasks');    
     }    
     public function indexFiltered()
     {
        $id=Auth::id();
        $user = User::find($id);
        $task = $user->tasks;
        $filtered=1;
         return view('tasks.index', compact('task','filtered'));
     }   
     
     public function need()
     {
        $task = Task::where('status', 0)->get();
        $filtered=1;
         return view('tasks.index', compact('task','filtered'));
     }    

     

    public function index()
    {

        $task = Task::all();
        return view('tasks.index',compact('task'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $task= new task();
        $id=Auth::id();
        $task->title = $request->title;
        $task->status=0;
        $task->user_id= $id;
        $task->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::find($id);
        return view(' tasks.edit',compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Are you a hacker or what?");}
        
        $task = Task::findOrFail($id);

        if(!$task->user->id == Auth::id()) return(redirect('tasks'));

        $task->update($request->except(['_token']));

        if($request->ajax()){
            return Response::json(array('result' => 'success1','status' => $request->status ), 200);   
        } else {          
            return redirect('tasks');           
        }
 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Are you a hacker or what?");}
        $task = Task::find($id);
        $task->delete();
        return redirect('tasks');
    }

  
}
